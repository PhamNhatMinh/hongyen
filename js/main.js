
jQuery(document).ready(function() {

    //Show toggle content tab in detail product
    var lblToggle = $('.horse-title');
    var pos_info = $('.info-product');
    var height_info = $('.info-product').outerHeight();
    if ( height_info > 1000) {
        lblToggle.removeClass('hidden');
        lblToggle.click(function(e){
            e.stopPropagation();            
            $('.wrap-content-detail').slideToggle('fast', function(){
                if($(this).is(":hidden")) {
                    lblToggle.children().text('Xem chi tiết bài viết');
                } else {
                    lblToggle.children().text('Ẩn chi tiết bài viết');
                }
            });
            $('.wrap-content-detail').removeClass('hidden');
        });  
    }

    // Menu responsive
    $('#trapNav').slicknav({
        label: ''
    });

    // Hero slider
	$("#productShow").slick({
		arrows: true,
		dots: true,
		autoplay: false,
        autoplaySpeed: 2000,
        pauseOnHover: true,
  	});
    
    //News slider
	$("#boxNews").slick({		 
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
        speed: 250,
        autoplaySpeed: 3000,
		autoplay: false,
		arrows: true,
        variableWidth: true,
        prevArrow: '<div class="blank-prev"><button type="button" class="slick-prev">Previous</button></div>',
        nextArrow: '<div class="blank-next"><button type="button" class="slick-next">Next</button></div>',
	});

    // Related accessories slider in Detail page
    $("#phukientuongthich, #spcunggia").slick({        
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplaySpeed: 3000,
        autoplay: false,
        arrows: true,
        variableWidth: true,
        rows: 2,
        prevArrow: '<div class="blank-prev"><button type="button" class="slick-prev">Previous</button></div>',
        nextArrow: '<div class="blank-next"><button type="button" class="slick-next">Next</button></div>',     
    }); 
        
    // Fancy box video
	var title_video = $(".video-item, .smvideo");
    if (title_video.length > 0) {
        title_video.fancybox({
            iframe: {
                scrolling: 'no',
                preload: true
            },
            autoCenter: true,
            autoResize: true,
            fitToView: true,
            autoHeight: true,
            autoWidth: true,
            openEffect: 'elastic',
            closeEffect: 'fade',
            nextEffect: 'elastic',
            prevEffect: 'fade',
            padding: 0,
            nextClick: true,
            prevClick: true
        });
    };

    //For tab content menu
    $('.dropdown-menu .nav-tabs > li').bind('touchstart', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).find('a').tab('show');
    });

    $('.dropdown-menu .nav-tabs > li').mouseover( function(){
        $(this).find('a').tab('show');
    });
    
    $('.dropdown-menu .nav-tabs > li').mouseout( function(){
        $(this).find('a').tab('hide');
    });

    //For hover menu active
    $('.navbar-nav > li.dropdown').mouseover( function(){
        var $this = $(this);
        $this.addClass('nav-active');
    });
    $('.navbar-nav > li.dropdown').mouseout( function(){
        var $this = $(this);
        $this.removeClass('nav-active');
    });

    //For equal height content left in sub menu
    var heightColNav = $('#contentNav ul.nav  .dropdown-menu').height();
    $('#contentNav .nav-tabs').css({'border-right':'1px solid #ccc', 'height':heightColNav});
    //Scroll top page
     $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('#scrollTop').fadeIn('slow');
        } else {
            $('#scrollTop').fadeOut('slow');
        }
    });

    $('#scrollTop a').click(function(event){
        event.preventDefault();
        event.stopPropagation();
        $('html,body').animate({ scrollTop:0},1000);
        return false;
    });

    //For fix top NavBar
    $('#contentNav .dropdown-menu').css('width',$('#trapNav').width());
    $(window).on('scroll', function () {
        var pos_window = $(window).scrollTop();
        var pos_header = $('.content-header').offset().top;
        var pos_trapNav = $('#trapNav').offset().left;
        var nav = $('#mainNav');
        if (pos_window > pos_header) {
            $('#detailNav').removeClass('nopadding');
            nav.addClass('navbar-fixed-top inyellow');
            nav.css({'margin-top': '0'});
            $('#contentNav .dropdown-menu').css('left',pos_trapNav);
        } else {
            nav.removeClass('navbar-fixed-top');
            nav.removeClass('navbar-fixed-top inyellow');
            $('#contentNav .dropdown-menu').css('left','0');
            $('#detailNav').addClass('nopadding');
        }
    });

    // Scroll paginage product
    if ($('#listProd').length > 0) {
        var ias = $.ias({
          container:  "#listProd",
          item:       ".item-product",
          pagination: "#pagination",
          next:       ".next"
        });
        // Add a loader image which is displayed during loading
        ias.extension(new IASSpinnerExtension());

        // Add a link after page 2 which has to be clicked to load the next page
        ias.extension(new IASTriggerExtension({offset: 2}));

        // Add a text when there are no more pages left to load
        ias.extension(new IASNoneLeftExtension({text: "You reached the end"}));
    }

    // Product slider in detail page
    var sync1 = $("#sync1");
    var sync2 = $("#sync2");
     
    sync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        navigation: true,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });
     
    sync2.owlCarousel({
        items : 5,
        itemsDesktop      : [1170,5],
        itemsDesktopSmall     : [979,5],
        itemsTablet       : [768,5],
        itemsMobile       : [479,5],
        pagination:false,
        responsiveRefreshRate : 100,
        afterInit : function(el){
          el.find(".owl-item").eq(0).addClass("synced");
        }
    });
     
    function syncPosition(el){
        var current = this.currentItem;
        $("#sync2")
          .find(".owl-item")
          .removeClass("synced")
          .eq(current)
          .addClass("synced")
        if($("#sync2").data("owlCarousel") !== undefined){
          center(current)
        }
      }
     
    $("#sync2").on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo",number);
    });
     
    function center(number){
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for(var i in sync2visible){
          if(num === sync2visible[i]){
            var found = true;
          }
        }
     
        if(found===false){
          if(num>sync2visible[sync2visible.length-1]){
            sync2.trigger("owl.goTo", num - sync2visible.length+2)
          }else{
            if(num - 1 === -1){
              num = 0;
            }
            sync2.trigger("owl.goTo", num);
          }
        } else if(num === sync2visible[sync2visible.length-1]){
          sync2.trigger("owl.goTo", sync2visible[1])
        } else if(num === sync2visible[0]){
          sync2.trigger("owl.goTo", num-1)
        }
        
    }
    // End product slider in detail page

});